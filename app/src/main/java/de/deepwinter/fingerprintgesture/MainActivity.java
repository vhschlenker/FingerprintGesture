package de.deepwinter.fingerprintgesture;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private static String LOGTAG = MainActivity.class.getSimpleName();

    Button btn_service_disabled_go_to_accessibilitysettings_to_enable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn_service_disabled_go_to_accessibilitysettings_to_enable = (Button) findViewById(R.id.btn_service_disabled_go_to_accessibilitysettings_to_enable);


        btn_service_disabled_go_to_accessibilitysettings_to_enable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(android.provider.Settings.ACTION_ACCESSIBILITY_SETTINGS);
                startActivityForResult(intent, 0);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isTheFingerprintGestureServiceEnabled(getApplicationContext())) {
            btn_service_disabled_go_to_accessibilitysettings_to_enable.setVisibility(View.VISIBLE);
        } else {
            btn_service_disabled_go_to_accessibilitysettings_to_enable.setVisibility(View.GONE);
        }
    }

    private static boolean isTheFingerprintGestureServiceEnabled(Context context) {
        ComponentName compName = new ComponentName(context, FingerprintGestureService.class);
        String flatName = compName.flattenToString();
        String enabledList = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
        return enabledList != null && enabledList.contains(flatName);
    }
}
