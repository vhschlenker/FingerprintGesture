package de.deepwinter.fingerprintgesture;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.FingerprintGestureController;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.widget.Toast;

import static android.accessibilityservice.FingerprintGestureController.*;

public class FingerprintGestureService extends AccessibilityService {
    private static String SERVICELOGTAG = FingerprintGestureService.class.getSimpleName();

    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();
        Log.d(SERVICELOGTAG,"Service connected");

        FingerprintGestureController gestureController = getFingerprintGestureController();
        Log.d(SERVICELOGTAG, "Is available: " + gestureController.isGestureDetectionAvailable() );

        FingerprintGestureController.FingerprintGestureCallback callback = new
                FingerprintGestureController.FingerprintGestureCallback() {
                    @Override
                    public void onGestureDetectionAvailabilityChanged(boolean available) {
                        super.onGestureDetectionAvailabilityChanged(available);
                    }

                    @Override
                    public void onGestureDetected(int gesture) {
                        super.onGestureDetected(gesture);
                        switch (gesture) {
                            case FINGERPRINT_GESTURE_SWIPE_DOWN:
                                performGlobalAction(GLOBAL_ACTION_NOTIFICATIONS);
                                break;
                            case FINGERPRINT_GESTURE_SWIPE_LEFT:
                                performGlobalAction(GLOBAL_ACTION_RECENTS);
                                break;
                            case FINGERPRINT_GESTURE_SWIPE_RIGHT:
                                performGlobalAction(GLOBAL_ACTION_BACK);
                                break;
                            case FINGERPRINT_GESTURE_SWIPE_UP:
                                performGlobalAction(GLOBAL_ACTION_HOME);
                                break;
                            default:
                                Log.e(SERVICELOGTAG, "Error: Unknown gesture type detected!");
                                break;
                        }
                    }
                };

        gestureController.registerFingerprintGestureCallback(callback, null);
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        Log.d(SERVICELOGTAG,"Event: " + event);
    }

    @Override
    public void onInterrupt() {
        Log.d(SERVICELOGTAG,"Service interrupted");
    }
}
